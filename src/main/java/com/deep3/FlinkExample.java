package com.deep3;

import com.deep3.map.TweetFlatMapper;
import com.deep3.map.TweetNormaliser;
import com.deep3.map.TweetSentimentAnalysis;
import com.deep3.model.ExtractedTweet;
import com.deep3.sink.ExtractedTweetElasticSink;
import com.deep3.source.TwitterEndPoint;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.twitter.TwitterSource;

import java.io.FileInputStream;
import java.util.Properties;

public class FlinkExample {

    public static void main(String... args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        Properties twitterProps = new Properties();

        twitterProps.load(new FileInputStream("twitter.properties"));

        String searchTerms = (String) twitterProps.get("search.props");

        TwitterSource twitterSource = new TwitterSource(twitterProps);
        TwitterEndPoint twitterEndPoint = new TwitterEndPoint(searchTerms.split(","));
        twitterSource.setCustomEndpointInitializer(twitterEndPoint);
        DataStream<String> streamSource = env.addSource(twitterSource);
        DataStream<ExtractedTweet> jsonTweets = streamSource.flatMap(new TweetFlatMapper())
                .map(new TweetSentimentAnalysis())
                .map(new TweetNormaliser());


        jsonTweets.addSink(new ExtractedTweetElasticSink(twitterProps.getProperty("elastic.url"), twitterProps.getProperty("elastic.index"),
                twitterProps.getProperty("elastic.type")));

        String app_name = String.format("Streaming Tweets");
        env.execute(app_name);

    }
}
