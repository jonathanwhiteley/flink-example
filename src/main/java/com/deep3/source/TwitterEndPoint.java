package com.deep3.source;

import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.endpoint.StreamingEndpoint;
import org.apache.flink.streaming.connectors.twitter.TwitterSource;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class TwitterEndPoint implements Serializable, TwitterSource.EndpointInitializer {

    private List<String> terms;

    public TwitterEndPoint(String ... terms) {
        this.terms = Arrays.asList(terms);
    }



    @Override
    public StreamingEndpoint createEndpoint() {
        StatusesFilterEndpoint endpoint =  new StatusesFilterEndpoint();

        endpoint.trackTerms(terms);
        return endpoint;
    }
}
