package com.deep3.sink;

import com.google.gson.Gson;
import io.github.openunirest.http.HttpResponse;
import io.github.openunirest.http.JsonNode;
import io.github.openunirest.http.Unirest;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class HttpSink extends RichSinkFunction<LinkedHashMap<String, Integer>> {

    private final String url;

    private final String index;

    private final String type;



    public HttpSink(String url, String index, String type) {
        this.url = url;
        this.index = index;
        this.type = type;
    }


    @Override
    public void invoke(LinkedHashMap<String, Integer> value) throws Exception {

        Map<String,Object> entity = new HashMap<>();

        entity.put("values", value);
        entity.put("time",  DateTimeFormatter.ISO_DATE_TIME.format(LocalDateTime.now()));

        Gson gson = new Gson();
        String entityJson = gson.toJson(entity);

        HttpResponse<JsonNode> response = Unirest.post(url+"/"+index+"/"+type).header("Content-Type","application/json")
                .body(entityJson).asJson();

        System.out.println("Response " + response.getStatus());
        System.out.println(response.getStatusText());
        System.out.println(response.getBody());

    }
}
