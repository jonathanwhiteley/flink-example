package com.deep3.sink;

import com.deep3.model.ExtractedTweet;
import com.google.gson.Gson;
import io.github.openunirest.http.HttpResponse;
import io.github.openunirest.http.JsonNode;
import io.github.openunirest.http.Unirest;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class ExtractedTweetElasticSink extends RichSinkFunction<ExtractedTweet> {

    private final String url;

    private final String index;

    private final String type;



    public ExtractedTweetElasticSink(String url, String index, String type) {
        this.url = url;
        this.index = index;
        this.type = type;
    }

    @Override
    public void invoke(ExtractedTweet value) throws Exception {

        Gson gson = new Gson();
        String entityJson = gson.toJson(value);

        HttpResponse<JsonNode> response = Unirest.post(url+"/"+index+"/"+type).header("Content-Type","application/json")
                .body(entityJson).asJson();

        if(response.getStatus() > 399) {
            System.out.println("Failed to index: " + response.getStatus());
        }
    }

}
