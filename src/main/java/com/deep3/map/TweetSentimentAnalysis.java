package com.deep3.map;

import com.deep3.model.ExtractedTweet;
import com.deep3.sentiment.SentimentAnalyser;
import org.apache.flink.api.common.functions.MapFunction;

public class TweetSentimentAnalysis implements MapFunction<ExtractedTweet, ExtractedTweet> {

    @Override
    public ExtractedTweet map(ExtractedTweet value) throws Exception {
        int sentiment = SentimentAnalyser.findSentiment(value.getText());

        value.setSentiment(sentiment);
        value.setText(null); //Get rid of original tweet

        return value;
    }
}
