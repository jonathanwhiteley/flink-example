package com.deep3.map;

import com.deep3.model.ExtractedTweet;
import org.apache.flink.api.common.functions.MapFunction;

import java.util.List;
import java.util.stream.Collectors;

public class TweetNormaliser implements MapFunction<ExtractedTweet, ExtractedTweet> {

    @Override
    public ExtractedTweet map(ExtractedTweet value) throws Exception {

        if (value.getTags() != null) {
          List<String> normalisedTags = value.getTags().stream().map(String::toLowerCase).collect(Collectors.toList());

          value.setTags(normalisedTags);
        }

        if (value.getLanguage() != null) {

            String language = value.getLanguage();

            language = language.toLowerCase();

            language = language.split("-")[0];

            value.setLanguage(language);
        }

        return value;
    }
}
