package com.deep3.map;

import com.deep3.model.ExtractedTweet;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.flink.util.Collector;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class TweetFlatMapper implements FlatMapFunction<String, ExtractedTweet> {
    @Override
    public void flatMap(String value, Collector<ExtractedTweet> out) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode;
        String tweetString = null;

        try {
            jsonNode = mapper.readValue(value, JsonNode.class);
            tweetString = jsonNode.get("text").textValue();
        } catch (Exception e) {
            //Do nothing
        }

        if (tweetString != null) {


            List<String> tags;

            Object parsedTweet = Configuration.defaultConfiguration().jsonProvider().parse(value);

            tags = JsonPath.read(parsedTweet, "$.entities.hashtags[*].text");

            ExtractedTweet extractedTweet = new ExtractedTweet();

            extractedTweet.setTags(tags);

            String dateInMillis = JsonPath.read(parsedTweet, "$.timestamp_ms");

            if (dateInMillis != null) {
                String dateFormated = DateTimeFormatter
                        .ISO_OFFSET_DATE_TIME
                        .format(Instant.ofEpochMilli(Long.valueOf(dateInMillis))
                                .atZone(ZoneId.systemDefault()));
                extractedTweet.setDate(dateFormated);
            }

            String language = JsonPath.read(parsedTweet, "$.user.lang");

            if(language != null )  {
                extractedTweet.setLanguage(language);
            }

            extractedTweet.setText(tweetString);

            out.collect(extractedTweet);
        }
    }
}
