package com.deep3.map;

import com.deep3.ValueComparator;
import org.apache.commons.collections.map.HashedMap;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.util.*;

public class MostPopularTags implements AllWindowFunction<Tuple2<String, Integer>, LinkedHashMap<String, Integer>, TimeWindow> {
    private final int limit;

    public MostPopularTags(int limit){
        this.limit = limit;
    }

    @Override
    public void apply(TimeWindow window, Iterable<Tuple2<String, Integer>> values, Collector<LinkedHashMap<String, Integer>> out) throws Exception {
        Map<String, Integer> valueMap = new HashMap<>();


        for(Tuple2<String,Integer> tuple: values){
            int count = 0;

            if(valueMap.containsKey(tuple.f0)) {
                count = valueMap.get(tuple.f0);
            }

            valueMap.put(tuple.f0, count + tuple.f1);
        }


        Comparator<String> comparator = new ValueComparator(valueMap);

        Map<String, Integer> sortedMap = new TreeMap<>(comparator);

        sortedMap.putAll(valueMap);

        LinkedHashMap<String, Integer> sortedTopN = sortedMap.entrySet()
                .stream()
                .limit(limit)
                .collect(LinkedHashMap::new,(m,e) -> m.put(e.getKey(), e.getValue()), Map::putAll);

        out.collect(sortedTopN);
    }
}
