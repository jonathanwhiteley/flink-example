package com.deep3.sentiment;

import org.junit.Test;

public class SentimentAnalyserTest {

    @Test
    public void testPositive(){
        String input = "Happy! This is good.";
        int sentiment = SentimentAnalyser.findSentiment(input);

        System.out.println(sentiment);
    }

    @Test
    public void testNegative(){
        String input = "Sad! This is bad.";

        int sentiment = SentimentAnalyser.findSentiment(input);

        System.out.println(sentiment);
    }

}
